package com.javatcup.springbootdemo.controller;


import com.javatcup.springbootdemo.domain.User;
import com.javatcup.springbootdemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.Random;

@RestController
@RequestMapping("/v1")
public class ControllerDemo {
    @Autowired
    UserService  userService;

    @GetMapping("/hello")
    public User helloWorld(){

        User user = new User();
        user.setName("Sri Harish");
        user.setNumber("1234567890");
        user.setGender("Male");
        user.setMoney("100");

        return user;
    }
    @PostMapping(value = "/user")
    public User getUserDetails(@RequestBody User user){
        System.out.println(user.toString());
        user.setMoney("1000");
        return user;
    }
    @PostMapping("/create")
    public int createNewUser(@RequestBody User user){
        user.setId(String.valueOf(new Random().nextInt()));
        return userService.saveUser(user);
    }
    @GetMapping("/getUsers")
    public List<User> getUsers(){
        return userService.getUsers();
    }
    @GetMapping("/getUser/{id}")
    public Optional<User> getUserDetails(@PathVariable String id){
        return userService.getUser(id);
    }
    @GetMapping("/getname/{name}")
    public List<User> getUseByName(@PathVariable String name){
        return userService.getUserByName(name);
    }
    @DeleteMapping("/delete")
    public int deleteUser(@RequestBody User user){
        return userService.deleteUser(user);
    }

    @PostMapping("/update")
    public Optional<User> updateUser(@RequestBody User user) throws Exception {
        return userService.updateUser(user);
    }

    @PostMapping("/getUser")
    public List<User> getUserDetail(@RequestBody User user){
        return userService.getUser(user);
    }

}
