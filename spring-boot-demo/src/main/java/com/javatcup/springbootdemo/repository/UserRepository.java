package com.javatcup.springbootdemo.repository;

import com.javatcup.springbootdemo.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<User,String> {
    List<User>  findByName(String name);
    @Query("{name : ?0, gender : ?1}")
    List<User>  findByNameAndGenderQuery(String name,String gender);
    @Query("{'$or' : [{'name' : ?0}, {'gender' : ?1}]}")
    List<User>  findByNameOrGenderQuery(String name,String gender);


//    Equal to.
//    Not equal to.
//    Less than / greater than.
//    Like.
//            Between.
//            Null / not Null.
//    Regular expressions.
//    and and or logical operators.
//            https://lishman.io/spring-data-mongodb-repository-queries

}
