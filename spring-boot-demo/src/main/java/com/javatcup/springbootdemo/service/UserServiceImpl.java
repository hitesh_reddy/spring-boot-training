package com.javatcup.springbootdemo.service;

import com.javatcup.springbootdemo.domain.User;
import com.javatcup.springbootdemo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    UserRepository userRepository;
    @Override
    public int saveUser(User user) {
        userRepository.save(user);
        return 1;
    }
    public List<User> getUsers(){
        return userRepository.findAll();
    }
    public Optional<User> getUser(String id){
        return userRepository.findById(id);
    }
    public List<User> getUser(User user){
        //return userRepository.findByNameAndGenderQuery(user.getName(),user.getGender());
        return userRepository.findByNameOrGenderQuery(user.getName(),user.getGender());
    }
    public List<User> getUserByName(String name){
        return userRepository.findByName(name);
    }
    public int deleteUser(User user){
        userRepository.delete(user);
        return 1;
    }
    public Optional<User> updateUser(User user) throws Exception {
        Optional<User> existing_data = getUser(user.getId());
        if(!existing_data.isPresent())
            throw new Exception("Record not Found");

        userRepository.save(user);

        return getUser(user.getId());
    }

}
