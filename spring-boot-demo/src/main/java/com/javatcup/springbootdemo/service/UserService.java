package com.javatcup.springbootdemo.service;

import com.javatcup.springbootdemo.domain.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

public interface UserService {
    public int saveUser(User user);
    public List<User> getUsers();
    public Optional<User> getUser(String id);
    public List getUser(User user);
    public List<User> getUserByName(String name);
    public int deleteUser(User user);
    public Optional<User> updateUser(User user) throws Exception;
}
