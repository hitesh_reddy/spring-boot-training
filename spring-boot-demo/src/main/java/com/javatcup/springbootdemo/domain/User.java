package com.javatcup.springbootdemo.domain;

import org.springframework.data.annotation.Id;

public class User {
    @Id
    private String id;
    private String name;
    private String money;
    private String gender;
    private String number;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", money='" + money + '\'' +
                ", gender='" + gender + '\'' +
                ", number='" + number + '\'' +
                '}';
    }
}
